#!/bin/bash

_IMAGE="$1"

if ! [[ "$_IMAGE" =~ ^[0-9]+$ ]]
then
	echo "-------------------------------------------"
	echo "ERROR !!! Please specify valid image number"
	echo "-------------------------------------------"
	echo "1) baseimage-openjdk11_0.0.1"
	echo ""
else
	echo "Setting up VM "$_IMAGE

	if [ $_IMAGE -eq 1 ]
	then
		sudo docker-compose -f "baseimage-openjdk11_0.0.1/docker-compose.yml" build
	else
		echo "-------------------------------------------"
		echo "ERROR !!! Please specify valid image number"
		echo "-------------------------------------------"
    echo "1) baseimage-openjdk11_0.0.1"
		echo ""
	fi
fi
