#!/bin/bash

rm -rf ${DOCKER_VOLUME}/demo/*
rm -rf ${DOCKER_VOLUME}/serviceTest/*

cp -r ./DOCKER_VOLUME_TEMPLATE/* ${DOCKER_VOLUME}
